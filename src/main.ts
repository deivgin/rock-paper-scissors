import { Game } from "./game";
const game = new Game();

const playBtn = document.getElementById("playButton");

playBtn?.addEventListener("click", () => {
  console.log(game.play("scissors"));
});
