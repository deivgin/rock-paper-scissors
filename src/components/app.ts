import { Game } from "../game";

const game = new Game();

export default class App {
  constructor() {}

  render() {
    return `
        <div>
            <button>${game.computerSelection()}</button>
        </div>
        `;
  }
}
