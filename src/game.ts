export type GameSelection = "rock" | "paper" | "scissors";
export type GameStatus = "win" | "draw" | "loose" | null;

export const selection: GameSelection[] = ["rock", "paper", "scissors"];

export class Game {
  selection: GameSelection[];

  constructor() {
    this.selection = ["rock", "paper", "scissors"];
  }

  computerSelection() {
    return selection[Math.floor(Math.random() * 3)];
  }

  play(playerSelection: GameSelection): GameStatus {
    if (playerSelection === this.computerSelection()) {
      return "draw";
    }

    switch (playerSelection) {
      case "rock":
        return this.computerSelection() === "paper" ? "loose" : "win";

      case "paper":
        return this.computerSelection() === "scissors" ? "loose" : "win";

      case "scissors":
        return this.computerSelection() === "rock" ? "loose" : "win";

      default:
        return null;
    }
  }
}
